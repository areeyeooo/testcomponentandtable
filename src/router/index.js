import { createRouter, createWebHistory } from 'vue-router'
import HelloWorld from './components/HelloWorld.vue'
import Table1 from './components/Table.vue'
import Table2 from './components/Table2.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HelloWorld,
  },
  {
    path: '/',
    name: 'Table1',
    component: Table1,
  },
  {
    path: '/',
    name: 'Table2',
    component: Table2,
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
